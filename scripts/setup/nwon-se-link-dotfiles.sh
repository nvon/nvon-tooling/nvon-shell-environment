#!/bin/bash
set -euo pipefail

# source utils
source $(dirname $(dirname "${BASH_SOURCE[0]}"))/nwon-se-utils.sh

#######################################################
## Link dotfiles
#######################################################

linkFilesInFolder ${NWON_SE_BASE_USER_DIR:?}/dotfiles-linked-to-home ${HOME:?}
linkFilesInFolder ${NWON_SE_USER_DIR:?}/dotfiles-linked-to-home ${HOME:?}
