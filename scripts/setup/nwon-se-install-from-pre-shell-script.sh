#!/bin/bash
set -euo pipefail

source $(dirname $(dirname "${BASH_SOURCE[0]}"))/nwon-se-utils.sh

# installing stuff like custom zsh extensions
printDebug "Installing stuff from pre shell entrypoint"
linuxSetupInstallFromPreShellEntrypoint
