#!/bin/bash

export CLEAN_OUTPUT=false

# export path
source "${HOME}/.nwon-shell-environment"

# set environment from env file
source "${NWON_SE_BASE_DIR}/.env"

# set sub folders
export NWON_SE_SCRIPTS_DIR="${NWON_SE_BASE_DIR}/scripts"
export NWON_SE_BACKUP_DIR="${NWON_SE_BASE_DIR}/backup"
export NWON_SE_USERS_DIR="${NWON_SE_BASE_DIR}/users"

# Base user
export NWON_SE_BASE_USER_DIR="${NWON_SE_USERS_DIR}/base-user"

# set folder for NWON-shell-toolbox
export NWON_SHELL_TOOLBOX_LOCATION=${NWON_SE_SCRIPTS_DIR}/nwon-shell-toolbox

setUserHasSudoRights() {
    if groups $(whoami) | grep -q '\bsudo\b'; then
        sudoRights=1
    else
        sudoRights=0
    fi

    export NWON_SE_USER_HAS_SUDO_RIGHTS=$sudoRights
}

userHasSudoRights() {
    if [ $NWON_SE_USER_HAS_SUDO_RIGHTS = 0 ]; then
        echo "You don't have sudo rights"
        exit 1
    fi
}

linuxSetupValidUsers() {
    cd ${NWON_SE_USERS_DIR}

    users=$(ls -d */ | sed 's#/##')
    echo "${users}"
}

sourceShellToolbox() {
    ensureShellToolboxIsInstalled
    source ${NWON_SHELL_TOOLBOX_LOCATION}/bootstrap.sh
}

ensureShellToolboxIsInstalled() {
    if [ ! -f "${NWON_SHELL_TOOLBOX_LOCATION}/bootstrap.sh" ]; then
        git submodule update --init --recursive
        echo "NWON-Shell-Toolbox cloned!"
    fi
}

backupFile() {
    FILE_PATH=${1}
    NOW=$(date +"%Y-%m-%d_%I-%M-%S")
    FILE_NAME="$(basename -- $FILE_PATH)"

    if [ -f ${FILE_PATH} ] || [ -d ${FILE_PATH} ]; then
        logInfo "Backuping ${FILE_PATH}"
        cp -r ${FILE_PATH} ${NWON_SE_BACKUP_DIR}/${NOW}_${FILE_NAME} 2>/dev/null || :
    fi
}

printDebug() {
    style=${2:-}

    if [[ ${NWON_SE_DEBUG_MODE} -eq 1 ]]; then
        if [ -z "${style}" ]; then
            logInfo ${1}

        else
            if [ "${style}" = "success" ]; then
                logSuccess ${1}
            else
                logInfo ${1}
            fi
        fi
    fi
}

linkFile() {
    FILE_TO_LINK=${1}
    TARGET_FILE=${2}

    if [ -f ${FILE_TO_LINK} ] || [ -d ${FILE_TO_LINK} ]; then

        # only backup files as backuping directories leads to problems
        if [ -f ${FILE_TO_LINK} ]; then
            backupFile ${TARGET_FILE}
        fi

        logInfo "Linking ${FILE_TO_LINK} to ${TARGET_FILE}"

        if [ -f ${TARGET_FILE} ] || [ -d ${TARGET_FILE} ] || [ -L ${TARGET_FILE} ]; then
            rm -r ${TARGET_FILE}
        fi

        ln -s ${FILE_TO_LINK} ${TARGET_FILE}
    fi
}

linkFilesInFolder() {
    FOLDER_WITH_FILES_TO_LINK=${1}
    TARGET_FOLDER=${2}

    for file_path in ${FOLDER_WITH_FILES_TO_LINK}/.[^.]*; do
        [ -e "$file_path" ] || continue
        filename="$(basename -- $file_path)"
        linkFile $file_path ${TARGET_FOLDER}/$filename
    done
}

sourceFolder() {
    folderToSource=${1}

    # surpress no match error
    if [[ "$0" != "bash" ]]; then
        setopt +o nomatch
    fi

    if [ -d "${folderToSource}" ]; then
        fileExist="0"

        if ls ${folderToSource}/* &>/dev/null; then
            fileExist="1"
        fi

        if [[ "${fileExist}" == 1 ]]; then
            for file_path in ${folderToSource}/*; do
                printDebug "source ${file_path}"
                source "${file_path}"
            done
        fi

        fileExist="0"

        if ls ${folderToSource}/.* &>/dev/null; then
            fileExist="1"
        fi

        if [[ "${fileExist}" == 1 ]]; then
            for file_path in ${folderToSource}/.*; do
                printDebug "source ${file_path}"
                source "${file_path}"
            done
        fi
    fi

    # reenable no match error
    if [[ "$0" != "bash" ]]; then
        setopt nomatch
    fi
}

sourceSourceFiles() {
    SUB_PATH=${1}

    sourceFolder ${NWON_SE_BASE_USER_DIR}/${SUB_PATH}
    sourceFolder ${NWON_SE_BASE_USER_DIR}/${SUB_PATH}/ignored

    sourceFolder ${NWON_SE_USER_DIR}/${SUB_PATH}
    sourceFolder ${NWON_SE_USER_DIR}/${SUB_PATH}/ignored
}

sourceConstants() {
    File="constants.sh"

    if [ -f ${NWON_SE_USER_DIR}/${File} ]; then
        printDebug "source ${NWON_SE_USER_DIR}/${File}"
        source ${NWON_SE_USER_DIR}/${File}

    elif [ -f ${NWON_SE_BASE_USER_DIR}/${File} ]; then
        printDebug "source ${NWON_SE_BASE_USER_DIR}/${File}"
        source ${NWON_SE_BASE_USER_DIR}/${File}
    fi
}

linuxSetupSetUser() {
    CHOSEN_USER=${1}
    VALID_USERS="$(linuxSetupValidUsers)"

    if [ -z "${CHOSEN_USER}" ]; then
        logError "Please specify a valid user. Options: "$(${VALID_USERS} | perl -p -e "s/\n/, /")
        exit 1
    fi

    if [[ "${VALID_USERS}" == *"${CHOSEN_USER}"* ]]; then
        export NWON_SE_USER=${CHOSEN_USER}
    else
        logError "${CHOSEN_USER} is invalid. Please specify a valid environment. Options: "$(linuxSetupValidUsers | perl -p -e "s/\n/, /")
        exit 1
    fi

    printDebug "Using Linux Setup user ${NWON_SE_USER}" "success"
}

linuxSetupInstallFromPreShellEntrypoint() {
    SUB_PATH="pre-shell-install-entrypoint.sh"

    if [ -f ${NWON_SE_BASE_USER_DIR}/${SUB_PATH} ]; then
        source ${NWON_SE_BASE_USER_DIR}/${SUB_PATH}
    fi

    if [ -f ${NWON_SE_USER_DIR}/${SUB_PATH} ]; then
        source ${NWON_SE_USER_DIR}/${SUB_PATH}
    fi
}

# set variables that depends on user and environment
export NWON_SE_USER_DIR="${NWON_SE_USERS_DIR}/${NWON_SE_USER}"
export NWON_SE_USER_STATIC_DIR="${NWON_SE_USER_DIR}/static"

sourceShellToolbox

linuxSetupSetUser ${NWON_SE_USER}
setUserHasSudoRights

sourceConstants
