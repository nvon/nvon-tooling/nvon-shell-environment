#!/bin/bash

set -eo pipefail

source "$(dirname "${BASH_SOURCE[0]}")"/nwon-se-utils.sh

chosenUser=${1}
linuxSetupSetUser ${chosenUser}
