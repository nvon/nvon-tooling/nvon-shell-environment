#!/bin/bash
set -euo pipefail

SCRIPTS_DIR=$(dirname $(readlink -f "$0"))

source "${SCRIPTS_DIR}"/setup/nwon-se-create-config-file.sh

# copy main config file
if [ ! -f "${NWON_SE_BASE_DIR}/.env" ]; then
    cp "${NWON_SE_BASE_DIR}/.env.example" "${NWON_SE_BASE_DIR}/.env"
fi

# copy docker file
if [ ! -f "${NWON_SE_BASE_DIR}/docker/.env" ]; then
    cp "${NWON_SE_BASE_DIR}/docker/.env.example" "${NWON_SE_BASE_DIR}/docker/.env"
fi

source "${SCRIPTS_DIR}"/nwon-se-utils.sh

logInfo "Created ${NWON_SE_CONFIG_FILE} and stored path (${NWON_SE_BASE_DIR}) to NWON-shell-environment"

# install apt packages
if [ "${NWON_SE_USER_HAS_SUDO_RIGHTS}" == 1 ] && [ -z "${NWON_SE_NO_APT_ON_INSTALL+x}" ]; then
    printDebug "Installing Apt packages"
    ${NWON_SE_BASE_DIR}/scripts/setup/nwon-se-install-apt-packages.sh
else
    printDebug "Not installing Apt packages as user has no sudo rights"
fi

printDebug "Installing zsh and oh-my-zsh"
${NWON_SE_BASE_DIR}/scripts/setup/nwon-se-install-zsh.sh

printDebug "Installing stuff from pre shell entrypoint"
${NWON_SE_BASE_DIR}/scripts/setup/nwon-se-install-from-pre-shell-script.sh

printDebug "Linking dotfiles"
${NWON_SE_BASE_DIR}/scripts/setup/nwon-se-link-dotfiles.sh

logSuccess "NWON-shell-environment is installed 🥳🖥"
