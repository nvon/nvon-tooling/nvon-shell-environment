#!/bin/bash
set -euo pipefail

# source utils
source $(dirname $(dirname "${BASH_SOURCE[0]}"))/nwon-se-utils.sh

SOURCES_FILE=${NWON_SE_USER_ENVIRONMENT_STATIC_DIR}/apt/sources.list

cp /etc/apt/sources.list $SOURCES_FILE
