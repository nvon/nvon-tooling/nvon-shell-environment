#!/bin/bash
set -euo pipefail

# source utils
source $(dirname $(dirname "${BASH_SOURCE[0]}"))/nwon-se-utils.sh

PACKAGE_FILE=${NWON_SE_USER_DIR}/static/apt/package_list.lst

dpkg --merge-avail <(apt-cache dumpavail)
dpkg --clear-selections
dpkg --set-selections <$PACKAGE_FILE
apt-get dselect-upgrade
