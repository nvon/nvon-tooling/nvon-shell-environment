#!/bin/bash
set -euo pipefail

# source utils
source $(dirname $(dirname "${BASH_SOURCE[0]}"))/nwon-se-utils.sh

PACKAGE_FILE=${NWON_SE_USER_ENVIRONMENT_STATIC_DIR}/apt/package_list.lst

dpkg --get-selections >$PACKAGE_FILE
