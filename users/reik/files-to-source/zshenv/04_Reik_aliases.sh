####################
# Reminder
####################

alias find-uuid="ls -l /dev/disk/by-uuid"
alias hibernate="systemctl hibernate"
alias hibernate-status="systemctl status systemd-hibernate.service"

alias portainer-run="docker run -d -p 8000:8000 -p 9443:9443 --name portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce:latest"
alias portainer-stop="docker container stop portainer"
alias portainer-start="docker container start portainer"
alias portainer-rm-container="docker container rm portainer"

####################
# Helper
####################

alias nrf="npm run full-check"

# Awesome tool for graphically detecting big files
alias find-big-files="ncdu"

###########################
# Git
# Additions to https://kapeli.com/cheat_sheets/Oh-My-Zsh_Git.docset/Contents/Resources/Documents/index
###########################

# Need to setup git alias
# git config --global alias.upstream '!git push -u origin HEAD'
alias gpb="git upstream"

# Git merge origin develop
alias gmod="git merge origin/$(git_develop_branch)"

# Git merge origin main
alias gmom="git merge origin/$(git_main_branch)"

# Git merge origin production
alias gmop="git merge origin/$(git_production_branch)"

alias gcpr="gco $(git_production_branch)"

alias gcm="gco $(git_main_branch)"

###########################
# Frequently edited files
###########################

alias ehost="code ~/.ssh/config"
alias ealias="code ${NWON_SE_BASE_DIR:?}/users/reik/files-to-source/zshenv/04_Reik_aliases.sh"

###########################
# Git
###########################

# 10 biggest files in git repository
alias gitbig="git rev-list --objects --all | grep -f <(git verify-pack -v .git/objects/pack/*.idx| sort -k 3 -n | cut -f 1 -d " " | tail -10)"

alias mounta="sudo mount -a"
alias vs="open-in-vs-code.sh"

# Delete all local branches but master and the current one, only if they are fully merged with master.
alias br-delete-useless="git branch | grep -v "master" | grep -v ^* | xargs git branch -d"

# Delete all local branches but master and the current one.
alias br-delete-useless-force="git branch | grep -v "master" | grep -v "develop" | grep -v "main" | grep -v ^* | xargs git branch -D"

###########################
# Global
###########################

alias -g gre=" | grep "
