# constant files need to export the following variables as a string:
# NWON_SE_APT_PACKAGES
# NWON_SE_ZSH_PLUGINS

if isMacOs; then
    export NWON_SE_APT_PACKAGES=""
else
    export NWON_SE_APT_PACKAGES="bat \
            fzf"
fi

export NWON_SE_ZSH_PLUGINS="docker docker-compose git zsh-autosuggestions zsh-syntax-highlighting ubuntu dnf pyenv dirhistory copypath copyfile history aliases npm poetry sudo kubectl"
