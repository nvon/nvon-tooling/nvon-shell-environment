#!/bin/bash
set -euo pipefail

# install something before something shell related is installed

ZSH_CUSTOM_FOLDER=${HOME:?}/.oh-my-zsh/custom

# install pyenv-lazy plugin
rm -rf ${ZSH_CUSTOM_FOLDER}/plugins/pyenv-lazy &>/dev/null
git clone https://github.com/davidparsson/zsh-pyenv-lazy.git ${ZSH_CUSTOM_FOLDER}/plugins/pyenv-lazy

# install powerlevel 10k theme
rm -rf ${ZSH_CUSTOM_FOLDER}/themes/powerlevel10k &>/dev/null
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM_FOLDER}/themes/powerlevel10k
