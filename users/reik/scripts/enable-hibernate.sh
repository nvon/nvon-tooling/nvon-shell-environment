#!/bin/bash
set -euo pipefail

source $(dirname "${BASH_SOURCE[0]}")/../../../scripts/nwon-se-utils.sh

userHasSudoRights

# this script is meant as more like a How To and less like a script

echo "This is more a Readme than a script. It is maybe better to run the comments one by one."
echo ""
read -r -p "Do you want to continue? [yes/n] " should_continue

if [ ${should_continue} != "yes" ]; then
    exit 1
fi

# turn off swap file
sudo swapoff /swapfile

# create swap as big as RAM
sudo dd if=/dev/zero of=/swapfile bs=$(cat /proc/meminfo | grep MemTotal | grep -oh '[0-9]*') count=1024 conv=notrunc

# create swap file
sudo mkswap /swapfile
sudo swapon /swapfile

# find UUID of swap file
# sudo findmnt -no SOURCE,UUID -T /swapfile

sudo apt -y install uswsusp

sudo dpkg-reconfigure -pmedium uswsusp
# Yes without swap
# select drive not file
# Encrypt No

sudo systemctl edit systemd-hibernate.service

# Enter:
# [Service]
# ExecStart=
# ExecStart=/usr/sbin/s2disk
# ExecStartPost=/bin/run-parts -a post /lib/systemd/system-sleep
