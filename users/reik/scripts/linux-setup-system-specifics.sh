#!/bin/bash
set -euo pipefail

# source utils
source $(dirname "${BASH_SOURCE[0]}")/../../../scripts/nwon-se-utils.sh

ENVIRONMENT_PATH="${NWON_SE_USER_ENVIRONMENT_DIR}"
STATIC_FILE_PATH=${ENVIRONMENT_PATH}/static/files

if [ -f ${STATIC_FILE_PATH}/.NAScredentials ]; then
    backupFile ~/.NAScredentials

    logInfo "Moving .NAScredentials"

    if [ ! -f ${STATIC_FILE_PATH}/.NAScredentials ]; then
        cp -vf ${STATIC_FILE_PATH}/.NAScredentials.example ${STATIC_FILE_PATH}/.NAScredentials
    fi

    cp -vf ${STATIC_FILE_PATH}/.NAScredentials ~/.NAScredentials
fi

if [ -f ${STATIC_FILE_PATH}/fstab ]; then
    backupFile /etc/fstab

    logInfo "Moving fstab file"

    cp -vf ${STATIC_FILE_PATH}/fstab /etc/fstab
fi
