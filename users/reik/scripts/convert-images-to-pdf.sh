#!/bin/bash
set -euo pipefail

convert "*.{png,jpeg,tif,tiff}" -quality 100 outfile.pdf
