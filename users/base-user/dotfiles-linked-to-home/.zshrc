# Set name of the theme to load.
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.

ZSH_THEME="agnoster"

sourceSourceFiles "files-to-source/zshrc"

if [ -x /usr/bin/numlockx  ]; then
    /usr/bin/numlockx on
fi

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.

# turn string with plugins into an array zsh style
ARRAY_OF_PLUGINS=(${(@s: :)NWON_SE_ZSH_PLUGINS})
plugins=(${ARRAY_OF_PLUGINS})

source $ZSH/oh-my-zsh.sh

# User configuration

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
#

#####################
# Custom stuff

# indicator for vi mode
MODE_INDICATOR="%{$fg_bold[red]%}[CMD MODE]%{$reset_color%}"

# add node_scripts to path
export PATH=~/node_scripts/.bin/:$PATH

# add composer bin to path
export PATH=~/.composer/vendor/bin/:$PATH

# global composer packages
export PATH=~/.config/composer/vendor/bin/:$PATH

# add scripts to path
export PATH=${NWON_SE_BASE_DIR}/scripts/:$PATH
export PATH=${NWON_SE_BASE_DIR}/scripts/backup/:$PATH
export PATH=${NWON_SE_BASE_DIR}/scripts/setup/:$PATH

# source base user scripts
if [ -d "${NWON_SE_BASE_USER_DIR}/scripts" ]; then
    export PATH=${NWON_SE_BASE_USER_DIR}/scripts:$PATH
fi

# source user scripts
if [ -d "${NWON_SE_USER_DIR}/scripts" ]; then
    export PATH=${NWON_SE_USER_DIR}/scripts:$PATH
fi

# add pip installed packages to path
export PATH=~/.local/bin/:$PATH

# add puppet to path
export PATH=/opt/puppetlabs/bin:$PATH

# fix grep warning
alias egrep="/bin/egrep $GREP_OPTIONS"
unset GREP_OPTIONS

if [ ! -z ${ZSH_START_MILLISECONDS} ]; then
    endtime=$(($(date +%s%N)/1000000))
    startupTime=$(($endtime-$ZSH_START_MILLISECONDS))
    echo "zsh started in $startupTime milliseconds"
fi

#increase to node memory to 8gb
export NODE_OPTIONS="--max-old-space-size=8192"


