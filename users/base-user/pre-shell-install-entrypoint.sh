#!/bin/bash
set -euo pipefail

# install something before something shell related is installed

# add zsh-syntax-hightlighting plugin
rm -rf ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting &>/dev/null
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting

# add zsh-autosuggestions
rm -rf ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions &>/dev/null
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
