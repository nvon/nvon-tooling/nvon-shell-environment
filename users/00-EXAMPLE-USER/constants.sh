# constant files need to export the following variables as a string:
# NWON_SE_APT_PACKAGES
# NWON_SE_ZSH_PLUGINS

export NWON_SE_APT_PACKAGES=""

export NWON_SE_ZSH_PLUGINS="git zsh-autosuggestions zsh-syntax-highlighting"
